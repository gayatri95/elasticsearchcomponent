package com.example.elasticsearchcomponent

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.android.appcomponents.views.ElasticSearchActivity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {


    var queryHashMap: HashMap<String, String>? = HashMap<String, String>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        btnStart.setOnClickListener {
            val intent = Intent(this, ElasticSearchActivity::class.java)
            //Need to pass parameters ... Layout Type + END point + ..
            intent.putExtra("layout", "linear")
            intent.putExtra("baseUrl","https://api.github.com/search/")
            intent.putExtra("endPoint","repositories")

            //Need to pass query parameters here...
            queryHashMap?.put("q","searchTerm")

            intent.putExtra("map",queryHashMap)

            this.startActivity(intent)
        }

        btnStartWithGrid.setOnClickListener {
            val intent = Intent(this, ElasticSearchActivity::class.java)
            //Need to pass parameters ... Layout Type + END point + ..
            intent.putExtra("layout", "grid")
            intent.putExtra("baseUrl","https://api.github.com/search/")
            intent.putExtra("endPoint","repositories")

            //Need to pass query parameters here...
            queryHashMap?.put("q","searchTerm")
            this.startActivity(intent)
        }


    }


}